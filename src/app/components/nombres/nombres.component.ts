import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nombres',
  templateUrl: './nombres.component.html',
  styleUrls: ['./nombres.component.scss']
})


export class NombresComponent implements OnInit {
  form!: FormGroup;
  form2!: FormGroup;
  coleccionNombres: string[] = [];
  imprimirNombres!: string;
  seleccione: string = 'Seleccione';


  constructor(private fb: FormBuilder) { 
    this.construirFormulario();
    this.construirFormulario2();
  }

  ngOnInit(): void {
  }

  // MÉTODOS
  // Para construir el formulario
  construirFormulario(): void {
    this.form = this.fb.group({
      nombre: ['', [ Validators.required, Validators.pattern(/[\Sa-zA-ZñN\S]/) ]],
    })
  }

  construirFormulario2(): void {
    this.form2 = this.fb.group({
      nombres: ['', Validators.required],
      listaNombres: ['']
    })
  }

  // Verificar si el nombre no es valido
  get nombreNoValido() {
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched
  }

  
  // agregar nombres en el select
  addNombre(): void {
    // se agrega el valor de nombre al select
    this.form2.value.nombres = this.form.value.nombre.trim();
    // en la coleccionNombres que es un array se guardan los nombres
    this.coleccionNombres.push(this.form2.value.nombres)
    // limpiamos el formulario
    this.form.reset();
    console.log(this.coleccionNombres);
  }


  // Mostrar nombres en el textarea
  showNombres(): void {
    console.log(this.coleccionNombres);    
    this.form2.value.listaNombres = this.coleccionNombres.join('\n');
    this.imprimirNombres = this.form2.value.listaNombres
    console.log(this.imprimirNombres);    
  }  
}
